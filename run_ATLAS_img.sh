#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;

####### shifter ############
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c centos7 --swtype shifter;
# if [ $(env | grep "SHIFTER_RUNTIME") == 'SHIFTER_RUNTIME=1' ]
#     then echo "Running in container"
# else
#     echo "Error not in container...Exit..."
#     # exit
# fi

######  singularity ########
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/salt:latest -m /pscratch -r /srv/singularity_exec.sh

