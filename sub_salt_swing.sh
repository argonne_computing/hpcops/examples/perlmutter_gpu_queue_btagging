#!/bin/bash
#
#SBATCH --job-name=salt-test
#SBATCH --account=ATLAS-HEP-GROUP 
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --time=00:05:00

module load singularity
srun singularity exec -e --bind $PWD \
    /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/salt:latest \
    bash singularity_exec.sh
